const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

class BookError extends Error {
  constructor(num, prop) {
    super();
    this.name = "BookError";
    this.message = `not correct ${prop}  "book" ${num}`;
  }
}

const divCont = document.getElementById("root");
const bookList = document.createElement("ul");
bookList.style.listStyle = "none";
divCont.append(bookList);
let num = 0;
class ListBook {
  constructor() {
    this.bookConteiner = document.createElement("li");
    this.bookAutor = document.createElement("h2");
    this.bookName = document.createElement("p");
    this.bookPrice = document.createElement("p");
  }
  createElem() {
    this.bookConteiner.append(this.bookAutor);
    this.bookConteiner.append(this.bookName);
    this.bookConteiner.append(this.bookPrice);
  }

  render(conteiner = bookList) {
    this.createElem();
    conteiner.append(this.bookConteiner);
  }
}

class Inclooder extends ListBook {
  constructor(author, name, price) {
    super();
    num += 1;
    if (author == undefined) {
      throw new BookError(num, "author");
    }
    if (name == undefined) {
      throw new BookError(num, "name");
    }
    if (price == undefined) {
      throw new BookError(num, "price");
    }
    this.author = author;
    this.name = name;
    this.price = price;
  }
  createElem() {
    super.createElem();
    this.bookAutor.innerText = "Author" + " " + this.author;
    this.bookName.innerText = "Name" + ":" + " " + this.name;
    this.bookPrice.innerText = "Price" + ":" + " " + this.price + " " + "$";
  }
}
books.forEach((el) => {
  try {
    new Inclooder(el.author, el.name, el.price).render();
  } catch (e) {
    if (e.name === "BookError") {
      console.error(e);
    } else {
      throw e;
    }
  }
});
